//
//  main.m
//  WhoApp
//
//  Created by Mihai Olteanu on 26/06/14.
//  Copyright (c) 2014 Mihai Olteanu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
