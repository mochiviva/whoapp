//
//  AppDelegate.h
//  WhoApp
//
//  Created by Mihai Olteanu on 26/06/14.
//  Copyright (c) 2014 Mihai Olteanu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
